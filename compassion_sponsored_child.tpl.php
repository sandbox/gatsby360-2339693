<?php drupal_add_css(drupal_get_path('module', 'compassion_sponsored_child') . '/css/compassion.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE)); ?>
<?php drupal_add_js('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', 'external'); ?>
<?php drupal_add_js(drupal_get_path('module', 'compassion_sponsored_child') .'/scripts/enabler.js'); ?>
<?php drupal_add_js(drupal_get_path('module', 'compassion_sponsored_child') .'/scripts/compassion.js'); ?>
<div id="banner-wrap">
	<div id="logo-link"></div>
	<div id="number"></div>
	<div id="btn"></div>
	<div id="child"></div>
</div><!-- .#banner-wrap -->