# Compassion Sponsored Child Plugin
---

A Drupal module that displays the number of children sponsored through the Compassion program for the day as well as an image of the most recently sponsored child.

## Installation
---

You can use the following steps to install the Compassion Sponsored Child module:
1. Download the compressed module.
2. Extract the module file.
3. Upload the extracted ``compassion_sponsored_child`` folder to the ``/sites/all/modules/`` directory.
4. Activate the module through the Modules screen in the Drupal Admin.

## Usage
---

Once you have activated the module you can add it to your site by going to the Structure->Blocks section of the Drupal administration. From here you can assign the block to any of your defined regions.

## Notes
---

If you include multiple instances of the module on the same page, only one of them will display.

Browsers that have an ad-blocker installed and enabled may prevent the content from displaying.